<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/428', function () {
    return view('Juan');
});
//get = method (tempat tulis url (defaultnya itu get))

Route::get('/create-phone','PhoneController@create');
Route::post('/post-phone','PhoneController@store');
Route::get('/see-phone','PhoneController@index');
Route::get('/edit-phone/{id}','PhoneController@edit');
//kurung kurawal artinya lempar parameter
Route::post('/update-phone/{id}','PhoneController@update');
Route::get('/delete-phone/{id}','PhoneController@destroy');