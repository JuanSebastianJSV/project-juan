<?php

namespace App\Http\Controllers;

use App\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phones = Phone::all();
        // $phone = nama variabel (dalam variabel phone menyimpan semua data yang ada di Tabel Phone)
        return view('JuanView',compact('phones'));
        //compact adalah dengan membawa variable phone ke dalam view.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('juan2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Phone::create(['type'=>$request['type'],'price' =>$request['price']]);
        // request adalah sesuatu yg kita input dari view (form)
        // Phone::create itu adalah eloquent (sesuatu untuk mempermudah query)
        //Synthax:Model::create ['model' ==> $request['type'] (datangnya dari view)]
        //kurung siku berbentuk array jadi bisa nyimpen banyak
        return redirect('/see-phone');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone=Phone::find($id);
        return view('JuanView2',compact('phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Phone::find($id) -> update(['type' =>$request['type'],'price' =>$request['price']]);
        // find hanya bisa ngecek primary key dari database
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = Phone::find($id);
        $phone -> delete();
    }
}
